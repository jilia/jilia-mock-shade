var Device = require('zetta-device');
var util = require('util');

var configDefaults = {
  quiet: false,
  movementInterval: 500, // millis
  movementIncrement: 5,
  minPosition: 0,
  maxPosition: 100
};

// Name defaults to 'shade' if not provided
var Shade = module.exports = function(name, options) {
  Device.call(this);
  this.name = name || 'shade';

  var config = getConfig(options);
  if(config.quiet) {
    // quiet: `log` => `noop`
    log = function () { }
  }
  this._config = config;

  this._positionTimer; // setTimeout used for movement
  this.currentPosition = this._config.minPosition;
  this.minPosition = this._config.minPosition;
  this.maxPosition = this._config.maxPosition;
  this.mock_MovementIntervalMillis = config.movementInterval;
  this.mock_MovementIncrement = config.movementIncrement;
}

util.inherits(Shade, Device);

Shade.prototype.init = function(config) {
  config
    .name(this.name)
    .type('shade')
    .state('ready')
    .monitor('currentPosition')
    .map('mock_SetMovementIntervalMillis', this.setMovementInterval, [
      {name:'interval', type:'number'}
    ])
    .map('mock_SetMovementIncrement', this.setMovementIncrement, [
      {name:'increment', type:'number'}
    ])
    .map('Open', this.open)
    .map('Close', this.close)
    .map('SetPosition', this.setPosition, [{
      name: 'position', type:'number'
    }])
    .map('SetMaxPosition', this.setMaxPosition, [{
      name: 'maxPosition', type: 'number'
    }])
    .map('SetMinPosition', this.setMinPosition, [{
      name: 'minPosition', type: 'number'
    }])
    .when('ready', {
      allow: [
        'Open', 'Close', 'SetPosition',
        'SetMaxPosition', 'SetMinPosition',
        'mock_SetMovementIntervalMillis',
        'mock_SetMovementIncrement'
      ]
    });
};

Shade.prototype.setMovementInterval = function(interval, cb) {
  this._config.movementInterval = interval;
  cb();
};

Shade.prototype.setMovementIncrement = function(increment, cb) {
  this._config.movementIncrement = increment;
  cb();
};

Shade.prototype.open = function(cb) {
  log('Open');
  this.setPosition(this.minPosition, cb);
};

Shade.prototype.close = function(cb) {
  log('Close');
  this.setPosition(this.maxPosition, cb);
};

Shade.prototype.setMaxPosition = function(maxPosition, cb) {
  log('Set Max Position: ' + maxPosition);
  if(maxPosition <= this.minPosition) {
    var msg = 'ERROR: Max position must be less than current min position';
    log(msg);
    cb(new Error(msg))
  }
  this.maxPosition = maxPosition;
  if(this.currentPosition > maxPosition) {
    return this.setPosition(maxPosition, cb);
  }
  cb();
}

Shade.prototype.setMinPosition = function(minPosition, cb) {
  log('Set Min Position: ' + minPosition);
  if(minPosition >= this.maxPosition) {
    var msg = 'ERROR: Min position must be less than current max position';
    log(msg);
    cb(new Error(msg))
  }
  this.minPosition = minPosition;
  if(this.currentPosition < minPosition) {
    return this.setPosition(minPosition, cb);
  }
  cb();
}

Shade.prototype.setPosition = function(targetPosition, cb) {
  log('Set Position: ' + targetPosition);

  clearInterval(this._positionTimer);
  targetPosition = constrain(this, targetPosition);

  var increment = this._config.movementIncrement;
  if(this.currentPosition > targetPosition) {
    increment = -increment;
  }

  var self = this;
  this._positionTimer = setInterval(function(){
    updatePosition(self, targetPosition, increment);
  }, self._config.movementInterval);

  cb();
};

Shade.prototype.actionMethod = function(cb) {
  cb();
};

function updatePosition(shade, targetPosition, increment) {
  var nextPosition = shade.currentPosition + increment;
  if(increment > 0 && nextPosition > targetPosition) {
    nextPosition = targetPosition;
  }
  if(increment < 0 && nextPosition < targetPosition) {
    nextPosition = targetPosition;
  }
  shade.currentPosition = nextPosition;
}

function constrain(shade, position) {
  if(position > shade.maxPosition) return shade.maxPosition;
  if(position < shade.minPosition) return shade.minPosition;
  return position;
}

function log(message) {
  console.log(message);
}

function getConfig(options) {
  var result = {};

  options = options || {};
  if(typeof options === 'string') {
    try { options = JSON.parse(options); }
    catch(err) { console.log(err); }
  }

  Object.keys(configDefaults).forEach(function(prop){
    var override = options.hasOwnProperty(prop);
    result[prop] = override ? options[prop] : configDefaults[prop];
  });

  return result;
}